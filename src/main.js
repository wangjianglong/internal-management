import Vue from 'vue'
import App from './App'
import router from './router'
import ElemntUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './style/public.less';

Vue.use(ElemntUI);
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
