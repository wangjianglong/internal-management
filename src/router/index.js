import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', component: resolve => require(['../views/Index.vue'],resolve),  children: [
        {
          path: '/',
          component: resolve => require(['../views/redis/index/Index.vue'],resolve)
        },
        {
          path: '/redis-index',
          component: resolve => require(['../views/redis/index/Index.vue'],resolve)
        },
        {
          path: '/redis-index2',
          component: resolve => require(['../views/redis/index2/Index.vue'],resolve)
        },
        {
          path: '/file-index',
          component: resolve => require(['../views/files/index/Index.vue'],resolve)
        },
        {
          path: '/file-index2',
          component: resolve => require(['../views/files/index2/Index.vue'],resolve)
        },
      ]}
  ]
})
